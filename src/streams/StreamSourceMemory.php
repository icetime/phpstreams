<?php

class StreamSourceMemory implements StreamSourceBase {
	private $content;
	private $position;
	
	function __construct($data) {
		$this->content = $data;
		$this->position = 0;
	}
	
	function seekBytes($offset, $whence = SEEK_CUR) {
		switch ($whence) {
			case SEEK_SET:
				$this->position = $offset;
				break;
			case SEEK_CUR:
				$this->position += $offset;
				break;
			case SEEK_END:
				$this->position = strlen($this->content) + $offset;
				break;
		}
		if ($this->position < 0) $this->position = 0;
		if ($this->position >= strlen($this->content)) $this->position = strlen($this->content);
	}
	
	function skipBytes($count) {
		seekBytes($count, SEEK_CUR);
	}
	
	function readByte() {
		if ($this->isEOF()) {
			throw new OutOfBoundsException(__CLASS__.": could not read byte (EOF)");
		} else {
			return substr($this->content, $this->position++, 1);
		}
	}
	
	function readBytes($count) {
		$result = "";
		
		if ($this->position + $count > strlen($this->content)) {
			$this->position = strlen($this->content);
			throw new OutOfBoundsException(__CLASS__.": could not read bytes (EOF)");
		} else {
			while ($count--) {
				$result .= $this->readByte();
			}
			return $result;
		}
	}
	
	function isEOF() {
		return $this->position >= strlen($this->content);
	}
}

?>
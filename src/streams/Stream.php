<?php

abstract class Endianness {
	const Normal 	= false;
	const Reversed 	= true;
}

abstract class BinaryTypes {
	static function flags($type, $endian = Endianness::Normal, $length = -1) {
		return array("type" => $type, "endianness" => $endian, "length" => $length);
	}
	
	const INT8 		= 0;
	const INT8U 	= 1;
	const INT16 	= 2;
	const INT16U 	= 3;
	const INT32 	= 4;
	const INT32U 	= 5;
	const STRING 	= 6;
}

function StreamGetStructureSize($struct) {
	$result = 0;
	$keys = array_keys($struct);
	for ($i = 0; $i < count($keys); $i++) {
		switch ($struct[$keys[$i]]["type"]) {
			case BinaryTypes::INT8:
			case BinaryTypes::INT8U:
				$result += 1;
				break;
			case BinaryTypes::INT16:
			case BinaryTypes::INT16U:
				$result += 2;
				break;
			case BinaryTypes::INT32:
			case BinaryTypes::INT32U:
				$result += 4;
				break;
			case BinaryTypes::STRING:
				if (is_int($struct[$keys[$i]]["length"])) {
					$result += $struct[$keys[$i]]["length"];
				} else if (is_string($struct[$keys[$i]]["length"])) {
					throw new Exception("Structure size is dynamic");
				}
				break;
		}
	}
	return $result;		
}

function strToHex($string){
    $hex = '';
    for ($i=0; $i<strlen($string); $i++){
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0'.$hexCode, -2);
    }
    return strToUpper($hex);
}

?>
<?php

define('__ROOT__', dirname(__FILE__));
require_once(__ROOT__."/Stream.php");
require_once(__ROOT__."/StreamSourceBase.php");
require_once(__ROOT__."/StreamSourceMemory.php");
require_once(__ROOT__."/StreamSourceFile.php");

/**
 * Siehe File Comment
 */
class StreamReader
{    
    private $source = null;
    
    static function fromFileHandle($hFile)
    {
        $p = 10;
        return new StreamReader(new StreamSourceFile($hFile));
    }
    
    static function fromMemory($data)
    {
        return new StreamReader(new StreamSourceMemory($data));
    }
    
    private function __construct($base)
    {
        $this->source = $base;
    }
        
    /**
     * Gelaber über readBytes
     * 
     * @param int           $count      Anzahl an zu lesenden Bytes
     * @param Endianness    $endian     MSB oder LSB Formatierung
     * @return byte[]                   Angeforderte Daten
     */
    private function readBytes($count, $endian = Endianness::Normal)
    {
        $data = $this->source->readBytes($count);
        
        if ($endian == Endianness::Reversed)
        {
            $array = str_split($data);
            $rev = array_reverse($array);
            return implode($rev);
        }
        else
        {
            return $data;
        }
    }
    
    public function isEOF()
    {
        return $this->source->isEOF();
    }
    
    public function readStructure($struct)
    {
        $result = array();
        $keys = array_keys($struct);
        for ($i = 0; $i < count($keys); $i++)
        {
            switch ($struct[$keys[$i]]["type"]) {
                case BinaryTypes::INT8:
                    $result[$keys[$i]] = $this->readInt8();
                    break;
                case BinaryTypes::INT8U:
                    $result[$keys[$i]] = $this->readInt8U();
                    break;
                case BinaryTypes::INT16:
                    $result[$keys[$i]] = $this->readInt16($struct[$keys[$i]]["endianness"]);
                    break;
                case BinaryTypes::INT16U:
                    $result[$keys[$i]] = $this->readInt16U($struct[$keys[$i]]["endianness"]);
                    break;
                case BinaryTypes::INT32:
                    $result[$keys[$i]] = $this->readInt32($struct[$keys[$i]]["endianness"]);
                    break;
                case BinaryTypes::INT32U:
                    $result[$keys[$i]] = $this->readInt32U($struct[$keys[$i]]["endianness"]);
                    break;
                case BinaryTypes::STRING:
                    if (is_int($struct[$keys[$i]]["length"]))
                    {
                        $count = $struct[$keys[$i]]["length"];
                    }
                    else if (is_string($struct[$keys[$i]]["length"]))
                    {
                        $count = $result[$struct[$keys[$i]]["length"]];
                    }
                    $result[$keys[$i]] = $this->readString($count);
                    break;
            }
        }
        return $result;
    }
    
    public function readInt8()
    {
        $val = $this->readInt8U();              // 8 Bit Signed Int [-128, 127]
        
        if ($val > 127)
        {
            return $val - 256;
        }
        else
        {
            return $val;
        }
    }
    
    public function readInt8U()
    {
        return ord($this->source->readByte());  // 8 Bit Unsigned Int [0, 255]
    }
    
    public function readString($length)
    {
        $result = "";
        
        while ($length--)
        {
            $result .= chr($this->readInt8U());         
        }
        
        return $result;
    }
    
    public function seekBytes($count, $whence = SEEK_CUR)
    {
        $this->source->seekBytes($count, $whence);
    }
    
    public function skipBytes($count)
    {
        $this->source->skipBytes($count);
    }
    
    public function readInt16($reversed = Endianness::Normal)
    {
        $bytes = $this->readBytes(2, $reversed);
        $val = unpack("s", $bytes);
        return $val[1];                         // s: 16 Bit Signed Int [-32768, 32767]
    }
    
    public function readInt16U($reversed = Endianness::Normal)
    {
        $bytes = $this->readBytes(2, $reversed);
        $val = unpack("S", $bytes);
        return $val[1];                         // S: 16 Bit Unsigned Int [0, 65535]
    }
    
    public function readInt32($reversed = Endianness::Normal)
    {
        $bytes = $this->readBytes(4, $reversed);
        $val = unpack("l", $bytes);
        return $val[1];                         // l: 32 Bit Signed Int [−2147483648, 2147483647]
    }
    
    public function readInt32U($reversed = Endianness::Normal)
    {
        $bytes = $this->readBytes(4, 1 - $reversed);
        return $this->_uint32be($bytes);        // L: 32 Bit Unsigned Int [0, 4294967295]
    }
    
    // aus php.net unpack() comments, geschrieben von rogier
    // behandelt 32-Bit UInts auf 32- und 64-Bit Systemen gleich
    private function _uint32be($bin)
    {
        // $bin is the binary 32-bit BE string that represents the integer
        if (PHP_INT_SIZE <= 4)
        {
            list( , $h, $l) = unpack("n*", $bin);
            return ($l + ($h * 0x010000));
        }
        else
        {
            list( , $int) = unpack("N", $bin);
            return $int;
        }
    } 

}

?>
<?php

class StreamSourceFile implements StreamSourceBase {
	private $handle;
	private $size;
	
	function __construct($hFile) {
		$this->handle = $hFile;
		$stats = fstat($hFile);
		$this->size = $stats['size'];
	}
	
	function seekBytes($offset, $whence = SEEK_CUR) {
		fseek($this->handle, $offset, $whence);
	}
	
	function skipBytes($count) {
		if ($count > 0) {
			fseek($this->handle, $count, SEEK_CUR);
		}
	}
	
	public function readByte() {
		$val = fread($this->handle, 1);
		if ($val === false) {
			throw new OutOfBoundsException(__CLASS__.", Ressource ".$this->handle.": could not read byte");
		} else {
			return $val;
		}
	}
	
	public function readBytes($count) {
		$data = fread($this->handle, $count);
		if (strlen($data) < $count) {
			throw new OutOfBoundsException(__CLASS__.", Ressource ".$this->handle.": could not read requested amount of bytes");
		} else {
			return $data;
		}
	}
	
	public function isEOF() {
		if ($this->size > 0) {
			$pos = ftell($this->handle);
			return $pos >= $this->size;
		} else {
			return feof($this->handle);
		}
	}
}

?>
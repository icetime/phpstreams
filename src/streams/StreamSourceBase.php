<?php

interface StreamSourceBase {
	public function readByte();
	public function readBytes($count);
	public function skipBytes($count);
	public function seekBytes($offset, $whence);
	public function isEOF();
}

?>
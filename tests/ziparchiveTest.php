<?php

require_once(dirname(__FILE__).'/../src/streams/StreamReader.php');

$local_file_header = array(
    "signature"             => BinaryTypes::flags(BinaryTypes::INT32U),
    "version"               => BinaryTypes::flags(BinaryTypes::INT16U),
    "flags"                 => BinaryTypes::flags(BinaryTypes::INT16U),
    "method"                => BinaryTypes::flags(BinaryTypes::INT16U),
    "lastmodtime"           => BinaryTypes::flags(BinaryTypes::INT16U),
    "lastmoddate"           => BinaryTypes::flags(BinaryTypes::INT16U),
    "crc32"                 => BinaryTypes::flags(BinaryTypes::INT32U),
    "compressedsize"        => BinaryTypes::flags(BinaryTypes::INT32U),
    "uncompressedsize"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "filenamelength"        => BinaryTypes::flags(BinaryTypes::INT16U),
    "extrafieldlength"      => BinaryTypes::flags(BinaryTypes::INT16U),
    "filename"              => BinaryTypes::flags(BinaryTypes::STRING, null, "filenamelength"),
    "extrafield"            => BinaryTypes::flags(BinaryTypes::STRING, null, "extrafieldlength")
);

$central_directory_file_header = array(
    "signature"             => BinaryTypes::flags(BinaryTypes::INT32U),
    "madeByOS"              => BinaryTypes::flags(BinaryTypes::INT16U),
    "version"               => BinaryTypes::flags(BinaryTypes::INT16U),
    "flags"                 => BinaryTypes::flags(BinaryTypes::INT16U),
    "method"                => BinaryTypes::flags(BinaryTypes::INT16U),
    "lastmodtime"           => BinaryTypes::flags(BinaryTypes::INT16U),
    "lastmoddate"           => BinaryTypes::flags(BinaryTypes::INT16U),
    "crc32"                 => BinaryTypes::flags(BinaryTypes::INT32U),
    "compressedsize"        => BinaryTypes::flags(BinaryTypes::INT32U),
    "uncompressedsize"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "filenamelength"        => BinaryTypes::flags(BinaryTypes::INT16U),
    "extrafieldlength"      => BinaryTypes::flags(BinaryTypes::INT16U),
    "filecommentlength"     => BinaryTypes::flags(BinaryTypes::INT16U),
    "disknumberstart"       => BinaryTypes::flags(BinaryTypes::INT16U),
    "interalfileattr"       => BinaryTypes::flags(BinaryTypes::INT16U),
    "externalfileattr"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "reloffsetlocalhdr"     => BinaryTypes::flags(BinaryTypes::INT32U),
    "filename"              => BinaryTypes::flags(BinaryTypes::STRING, null, "filenamelength"),
    "extrafield"            => BinaryTypes::flags(BinaryTypes::STRING, null, "extrafieldlength"),
    "filecomment"           => BinaryTypes::flags(BinaryTypes::STRING, null, "filecommentlength")
);

$end_of_central_directory = array(
    "signature"             => BinaryTypes::flags(BinaryTypes::INT32U),
    "disknumber"            => BinaryTypes::flags(BinaryTypes::INT16U),
    "centraldirstartdisk"   => BinaryTypes::flags(BinaryTypes::INT16U),
    "centraldirstartoffset" => BinaryTypes::flags(BinaryTypes::INT16U),
    "numentries"            => BinaryTypes::flags(BinaryTypes::INT16U),
    "centraldirsize"        => BinaryTypes::flags(BinaryTypes::INT32U),
    "centraldiroffset"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "zipcommentlength"      => BinaryTypes::flags(BinaryTypes::INT16U),
    "zipcomment"            => BinaryTypes::flags(BinaryTypes::STRING, null, "zipcommentlength")
);

$zip64_descriptor = array(
    "crc32"                 => BinaryTypes::flags(BinaryTypes::INT32U),
    "compressedsizelo"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "compressedsizehi"      => BinaryTypes::flags(BinaryTypes::INT32U),
    "uncompressedsizelo"    => BinaryTypes::flags(BinaryTypes::INT32U),
    "uncompressedsizehi"    => BinaryTypes::flags(BinaryTypes::INT32U)
);

$MAGIC_CENTRAL_DIRECTORY_FILE_HEADER    = 0x02014B50;
$MAGIC_END_OF_CENTRAL_DIRECTORY         = 0x06054B50;
$MAGIC_LOCAL_FILE_HEADER                = 0x04034B50;

class ZipArchiveTest extends PHPUnit_Framework_TestCase
{
    function zip64sizeFromExtraData($extra) {
        global $zip64_descriptor;
        $stream = StreamReader::fromMemory($extra);
        $info = $stream->readStructure($zip64_descriptor);
        return $info["compressedsizelo"];
    }

    function testZipArchiveRead() {
        global $local_file_header;
        global $central_directory_file_header;
        global $end_of_central_directory;
        global $zip64_descriptor;

        global $MAGIC_LOCAL_FILE_HEADER;
        global $MAGIC_END_OF_CENTRAL_DIRECTORY;
        global $MAGIC_LOCAL_FILE_HEADER;

        $hZip = fopen(dirname(__FILE__)."/archive.zip", "rb");
        $this->assertTrue($hZip != false);
        $stream = StreamReader::fromFileHandle($hZip);

        if (isset($_GET["file"])) {
            $requested_file = $_GET["file"];
        } else {
            $requested_file = "";
        }

        while (!$stream->isEOF()) {
            $magic = $stream->readInt32U();
            $stream->seekBytes(-4);
            
            switch ($magic) {
                case $MAGIC_LOCAL_FILE_HEADER:
                    $info            = $stream->readStructure($local_file_header);
                    $compressed_size = $info["compressedsize"];
                    $current_file    = $info["filename"];

                    if (basename($current_file) == $requested_file) {
                        if ($compressed_size == 0xFFFFFFFF && $info["extrafieldlength"] == 20) {
                            $compressed_size = zip64sizeFromExtraData($info["extrafield"]);
                        }

                        $compressed_data = $stream->readString($compressed_size);
                        $uncompressed_data = gzinflate($compressed_data);

                        $fx = fopen($requested_file, "wb");
                        fwrite($fx, $uncompressed_data);
                        fclose($fx);

                        // crc32() returns signed 32 bit int ->
                        // (int) converts unsigned 32 bit int to signed 32 bit int
                        $this->assertEquals(crc32($uncompressed_data, (int)$info["crc32"]));
                    } else {
                        $stream->skipBytes($compressed_size);
                    }

                    break;

                ////////////////////////////////////////////////////////////////
                // skip other zip blocks
                case $MAGIC_CENTRAL_DIRECTORY_FILE_HEADER:
                    $info = $stream->readStructure($central_directory_file_header);
                    break;
            
                case $MAGIC_END_OF_CENTRAL_DIRECTORY:
                    $info = $stream->readStructure($end_of_central_directory);
                    break;
                
                default:
                    fclose($hZip);
                    $this->assertTrue(false);
                    
            }

        }

        $stream = null;
        fclose($hZip);
    }
}

?>
<?php

require_once(dirname(__FILE__).'/../src/streams/StreamReader.php');

class StreamReaderTest extends PHPUnit_Framework_TestCase {

	public function testCanReadID3v1() {
		$id3v1tag = array(
			"signature" => BinaryTypes::flags( BinaryTypes::STRING, null,  3 ),
			"title" 	=> BinaryTypes::flags( BinaryTypes::STRING, null, 30 ),
			"artist" 	=> BinaryTypes::flags( BinaryTypes::STRING, null, 30 ),
			"album" 	=> BinaryTypes::flags( BinaryTypes::STRING, null, 30 ),
			"year" 		=> BinaryTypes::flags( BinaryTypes::STRING, null,  4 ),
			"comment" 	=> BinaryTypes::flags( BinaryTypes::STRING, null, 30 ),
			"genre" 	=> BinaryTypes::flags( BinaryTypes::INT8U			 )
		);

		$hFile = fopen(dirname(__FILE__)."/music.mp3", "rb");
		$stream = StreamReader::fromFileHandle($hFile);
		$size = StreamGetStructureSize($id3v1tag);

		$stream->seekBytes(-$size, SEEK_END);
		$tags = $stream->readStructure($id3v1tag);
		fclose($hFile);

		$this->assertEquals($tags["signature"], "TAG");
	}
}

?>